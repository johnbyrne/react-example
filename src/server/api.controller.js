exports.notificationData = (req,res) => {
  setTimeout(() => {
    res.json({
      data: {
        notificationData: [{
          mdIcon: 'error_outline',
          message: 'A.Smith passed Maths level 2',
          details: 'Individual grades: A, A, B, B'
        },{
          mdIcon: 'build',
          message: 'T.Andrew failed Literacy level 3',
          details: 'Individual grades: C, D, D, E'
        },{
          mdIcon: 'wifi_tethering',
          message: 'S.Peters passed Worldwide level 4',
          details: 'Individual grades: A, A, A, C'
        }]
      }
    });
  }, 500)
};
