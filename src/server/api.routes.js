const express = require('express');
const apiController = require('./api.controller');
const router = express.Router(); // eslint-disable-line

router.route('/notificationData').get(apiController.notificationData);

module.exports = router;
