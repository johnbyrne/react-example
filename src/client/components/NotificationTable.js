import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { fetchNotificationData } from '../services/notifications'
// import './NotificationTable./scss'

class NotificationTable extends Component {

  constructor(props) {
    super(props)
  }

  componentWillMount() {
    this.props.fetchNotificationData()
  }

  generateHeaders(row) {
    if (row === undefined) {
      return;
    }
    var headingData = Object.keys(row).map((key, i) => {
      if (i === 0) {
        return <td key={'heading-' + i} className="annotation-bold brandingBg7"></td>
      } else {
        return <td key={'heading-' + i} className="annotation-bold brandingBg7">{key[0].charAt(0).toUpperCase() + key.slice(1)}</td>
      }
    })
    return <thead key={'thead'} className="header brandingBg5"><tr className="subheader">{headingData}</tr></thead>
  }

  generateRows(data) {
    return data.map((item, i) => {
      var notificationData = Object.keys(item).map((key, j) => {
        if (key === 'mdIcon') {
          return <td key={'notification-' + j}><i className="material-icons device-icon brandingText8">{item[key]}</i></td>
        } else {
          return <td key={'notification-' + j}> {item[key]} </td>
        }
      });
      switch (i % 2) {
        case 0:
          return <tr key={'row-' + i}>{notificationData}</tr>
          break
        case 1:
          return <tr key={'row-' + i} className="brandingBg5">{notificationData}</tr>
          break
      }
    })
  }

  render() {

    const { notificationData, isLoading } = this.props

    let headerComponents = this.generateHeaders(notificationData[0])
    let rowComponents = this.generateRows(notificationData)

    return (
      <div>
        {isLoading &&
          <h2>Loading...</h2>
        }
        {notificationData.length > 0 &&
          <div className="notification-table-container">
            <div className="row">
              <table>
                {/*{headerComponents}*/}
                <tbody>
                  {rowComponents}
                </tbody>
              </table>
            </div>
          </div>
        }
      </div>
    )
  }
}

NotificationTable.propTypes = {
  notificationData: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  fetchNotificationData: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {

  const { notificationDataState  } = state

  const {
    isLoading,
    notificationData
  } = notificationDataState || {
    isLoading: true,
    notificationData: []
  }

  return {
    isLoading,
    notificationData
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchNotificationData: () => {
      dispatch(fetchNotificationData())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationTable)
