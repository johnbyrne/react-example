import React, { Component, PropTypes } from 'react'

class CardToolbar extends Component {
  render() {
    return (
      <div className="row card-toolbar branding-bg-dark-cyan">
        <div className="row">
          <div className="col col-md-10">
            <div className="label">{this.props.title}</div>
            {/*<div className="description">Recent observations</div>*/}
          </div>
          {/*<div className="col col-md-2">
            <i className="pull-right material-icons">more_vert</i>
          </div>*/}
        </div>
      </div>
    )
  }
}

export default CardToolbar
