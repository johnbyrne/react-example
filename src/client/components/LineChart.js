import React, { Component } from 'react'
import d3 from 'd3'
import ReactDOM from 'react-dom'

class LineChart extends Component {

  componentDidMount() {
    let chart = {}
    let rate = 500
    let points =  20
    // let width = 500
    let width = ReactDOM.findDOMNode(this.refs.chart).clientWidth
    let height = 30
    let padding =  40
    let uphours =   0
    let x = d3.scale.linear().domain([0, 1]).range([0, width])
    let y = d3.scale.linear().domain([0, 1]).range([height, 0])
    let strokeColour = '#FFFFFF'

    function _getData () {
      return d3.range(points + 2).map(function(i) {return _getDataPoint(i)})
    }

    function _getDataPoint(i) {
      return { x: (i)/points, y: Math.random() * 3}
    }

    let initialData = _getData()

    chart = d3.select(ReactDOM.findDOMNode(this.refs.chart))
      .data([initialData])
      .append("svg:svg")
        .attr("width",  width)
        .attr("height", 120)
      .append("svg:g")
        .attr("transform", "translate(" + 0 + "," + (2 * padding) + ")")

    chart
      .append("svg:path")
        .attr("class", "line-" + 1)
        .attr("d", d3.svg.line()
          .x(function(d) {return x(d.x)})
          .y(function(d) {return y(d.y)})
          .interpolate("monotone")
        )

    chart.selectAll("dot")
      .data(initialData)
      .enter()
      .append("circle")
      .attr("class", "circle-" + 1)
      .attr('style', 'fill: ' + strokeColour + '')
      .attr("r", 3.5)
      .attr("cx", function(d) { return x(d.x) })
      .attr("cy", function(d) { return y(d.y) })
  }

  render() {
    return (
      <div ref="chart" className="row visualisation"></div>
    )
  }
}

export default LineChart
