import React, { Component } from 'react'
import d3 from 'd3'
import ReactDOM from 'react-dom'

class BarChart extends Component {

  componentDidMount() {
    let barData = [{
      'x': Math.floor(Math.random() * 100),
      'y': Math.floor(Math.random() * 100)
    }, {
      'x': Math.floor(Math.random() * 100),
      'y': Math.floor(Math.random() * 100)
    }, {
      'x': Math.floor(Math.random() * 100),
      'y': Math.floor(Math.random() * 100)
    }, {
      'x': Math.floor(Math.random() * 100),
      'y': Math.floor(Math.random() * 100)
    }, {
      'x': Math.floor(Math.random() * 100),
      'y': Math.floor(Math.random() * 100)
    }, {
      'x': Math.floor(Math.random() * 100),
      'y': Math.floor(Math.random() * 100)
    }, {
      'x': Math.floor(Math.random() * 100),
      'y': Math.floor(Math.random() * 100)
    }, {
      'x': Math.floor(Math.random() * 100),
      'y': Math.floor(Math.random() * 100)
    }, {
      'x': Math.floor(Math.random() * 100),
      'y': Math.floor(Math.random() * 100)
    }, {
      'x': Math.floor(Math.random() * 100),
      'y': Math.floor(Math.random() * 100)
    }]

    let vis = d3.select(ReactDOM.findDOMNode(this.refs.chart))
      .append("svg:svg")
      .attr("width", ReactDOM.findDOMNode(this.refs.chart).clientWidth)
      .attr("height", 140),
      WIDTH = ReactDOM.findDOMNode(this.refs.chart).clientWidth,
      HEIGHT = 140,
      MARGINS = {
        top: 30,
        right: 0,
        bottom: 0,
        left: 0
      },

      xRange = d3.scale.ordinal().rangeRoundBands([MARGINS.left, WIDTH - MARGINS.right], 0.1).domain(barData.map(function (d) {
        return d.x
      })),

      yRange = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([0,
        d3.max(barData, function (d) {
          return d.y
        })
      ])

    vis.selectAll('rect')
      .data(barData)
      .enter()
      .append('rect')
      .attr('x', function (d) {
        return xRange(d.x)
      })
      .attr('y', function (d) {
        return yRange(d.y) + 10
      })
      .attr('width', xRange.rangeBand())
      .attr('height', function (d) {
        return ((HEIGHT - MARGINS.bottom) - yRange(d.y))
      })
      .attr('fill', 'white')

  }

  render() {
    return (
      <div ref="chart" className="row visualisation"></div>
    )
  }
}

export default BarChart
