import fetch from 'isomorphic-fetch'
import {
  REQUEST_NOTIFICATION_DATA,
  RECEIVE_NOTIFICATION_DATA } from './serviceTypes'
  
import { API_SERVER_PATH } from '../config/settings'

const mapNotificationData = (notificationData) => {
  return notificationData
}

const requestNotificationData = () => {
  return {
    type: REQUEST_NOTIFICATION_DATA
  }
}

const receiveNotificationData = (json) => {
  var notificationData = json.data.notificationData.map(mapNotificationData)
  return {
    type: RECEIVE_NOTIFICATION_DATA,
    notificationData
  }
}

export const fetchNotificationData = () => {
  return (dispatch) => {
    dispatch(requestNotificationData())
    return fetch(`${API_SERVER_PATH}/notificationData`)
      .then((response) => response.json())
      .then((json) => {
        dispatch(receiveNotificationData(json))
      })
  }
}
