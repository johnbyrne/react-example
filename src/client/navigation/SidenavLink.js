import React, { Component, PropTypes } from 'react'
import {Link} from "react-router";

class SidenavLink extends Component {
  constructor(props, context){
    super(props);
    this.context = context
  }

  render() {
    let isActive = this.context.router.isActive(this.props.to, true),
      className = isActive ? "branding-text-pale-violet-red" : "branding-text-white",
      iconClass = "material-icons " + className,
      linkClass = "sidenav-link " + className

    return (
      <div className="sidenav-link-container branding-bg-light-sea-green">
        <Link to={this.props.to}>
          <div className={linkClass}>{this.props.text}</div>
        </Link>
      </div>
    );
  }
}

SidenavLink.contextTypes = {
  router: React.PropTypes.object
}

export default SidenavLink
