import React, { Component } from 'react'
import SecondaryToolbarLink from './SecondaryToolbarLink'
import { Link } from "react-router";

class SecondaryToolbar extends Component {
  constructor(props, context){
    super(props);
    this.context = context
  }

  render() {
    let links = [{
        path: '/register',
        icon: 'beenhere',
        name: 'REGISTER'
    },{
        path: '/make-observation',
        icon: 'find_in_page',
        name: 'MAKE OBSERVATION'
    },{
        path: '/planning',
        icon: 'class',
        name: 'PLANNING'
    },{
        path: '/daily-diary',
        icon: 'library_books',
        name: 'DAILY DIARY'
    }]

    let linkElements = links.map((link, index) => {
        return <SecondaryToolbarLink key={index} to={link.path} icon={link.icon} text={link.name} />
    })

    let isActive = this.context.router.isActive("dashboard", true),
      className = isActive ? "branding-text-pale-violet-red" : "branding-text-dark-cyan",
      iconClass = "material-icons " + className,
      linkClass = "secondary-toolbar-link branding-text-black"

    return (
      <div className="secondary-toolbar branding-bg-white">
        <div className="row">
          <div className="col-md-1"></div>
          <div className="col-md-2">
            <Link to="dashboard">
              <img className="secondary-toolbar-logo" src="src/client/images/logo.png" />
              <div className={linkClass}>BLOSSOMBOARD</div>
            </Link>
          </div>
          {linkElements}
        </div>
      </div>
    )
  }
}

SecondaryToolbar.contextTypes = {
  router: React.PropTypes.object
}

export default SecondaryToolbar
