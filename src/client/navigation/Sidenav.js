import React, { Component } from 'react'
import SidenavLink from './SidenavLink'

class Sidenav extends Component {
  render() {
    let links = [{
        path: '/maths',
        icon: 'equalizer',
        name: 'MATHS'
    },{
        path: '/world',
        icon: 'location_city',
        name: 'WORLD'
    },{
        path: '/literacy',
        icon: 'book',
        name: 'LITERACY'
    }]

    let linkElements = links.map((link, index) => {
        return <SidenavLink key={index} to={link.path} icon={link.icon} text={link.name} />
    })

    return (
      <div className="sidenav branding-bg-light-sea-green">
        {linkElements}
      </div>
    )
  }
}

export default Sidenav
