import React, { Component, PropTypes } from 'react'
import { Link } from "react-router";

class SecondaryToolbarLink extends Component {
  constructor(props, context){
    super(props);
    this.context = context
  }

  render() {
    let isActive = this.context.router.isActive(this.props.to, true),
      className = isActive ? "branding-text-pale-violet-red" : "branding-text-dark-cyan",
      iconClass = "secondary-toolbar-icon material-icons " + className,
      linkClass = "secondary-toolbar-link branding-text-black"

    return (
      <div className="secondary-toolbar-link-container col-md-2">
        <Link to={this.props.to}>
          <i className={iconClass}>{this.props.icon}</i>
          <div className={linkClass}>{this.props.text}</div>
        </Link>
      </div>
    );
  }
}

SecondaryToolbarLink.contextTypes = {
  router: React.PropTypes.object
}

export default SecondaryToolbarLink
