import React, { Component } from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import rootReducer from './reducers'
import { Router, Route, IndexRoute, IndexRedirect, browserHistory } from 'react-router'
import { syncHistory } from 'react-router-redux'
import { routerMiddleware, push } from 'react-router-redux'
import { DashboardPage, MakeObservationPage } from './pages'
import { PrimaryToolbar, SecondaryToolbar } from './navigation'

// const historyMiddleware = syncHistory(browserHistory)
const historyMiddleware = routerMiddleware(browserHistory)

// configure middleware & store
const createStoreWithMiddleware = applyMiddleware(
  thunkMiddleware,
  createLogger(),//,
  // applyMiddleware(historyMiddleware)
  historyMiddleware
)(createStore)

const configureStore = (initialState) => {
  const configuredStore = createStoreWithMiddleware(rootReducer, initialState)

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducers', () => {
      const nextRootReducer = require('./reducers')
      configuredStore.replaceReducer(nextRootReducer)
    })
  }
  return configuredStore
}

const store = configureStore()

class MainLayout extends Component {
  render() {
    return (
      <div className="main-container brandingBg4">
        <PrimaryToolbar />
        <SecondaryToolbar />
        <div className="page">
          {this.props.children}
        </div>
      </div>
    )
  }
}

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path='/' component={MainLayout}>
        <Route path='dashboard' component={DashboardPage}/>
        <Route path='make-observation' component={MakeObservationPage}/>
        <IndexRedirect to="/dashboard" />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)
