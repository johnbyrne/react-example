import React, { Component } from 'react'
import CardToolbar from '../../components/CardToolbar'
import LineChart from '../../components/LineChart'
import BarChart from '../../components/BarChart'

class DashboardPage extends Component {
  render() {
    return (
      <div>
        <div className="page">
          <div className="row">
            <div className="col col-md-3">
              <div className="card branding-bg-light-sea-green">
                <CardToolbar title="grades" />
                <LineChart />
              </div>
            </div>
            <div className="col col-md-4">
              <div className="card branding-bg-light-sea-green">
                <CardToolbar title="attendance" />
                <LineChart />
              </div>
            </div>
            <div className="col col-md-5">
              <div className="card branding-bg-light-sea-green">
                <CardToolbar title="absence" />
                <LineChart />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col col-md-5">
              <div className="card branding-bg-light-sea-green">
                <CardToolbar title="courses" />
                <BarChart />
              </div>
            </div>
            <div className="col col-md-7">
              <div className="card branding-bg-light-sea-green">
                <CardToolbar title="library" />
                <BarChart />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default DashboardPage
