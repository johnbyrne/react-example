import React, { Component } from 'react'
import CardToolbar from '../../components/CardToolbar'
import LineChart from '../../components/LineChart'
import NotificationTable from '../../components/NotificationTable'
import Sidenav from '../../navigation/Sidenav'
import { getActiveSidenavTab, setActiveSidenavTab } from './actions/activeSidenavTab'

class MakeObservationPage extends Component {
  render() {
    return (
      <div>
        <div className="page">
          <div className="row">
            <div className="col col-md-1 sidenav-container branding-bg-light-sea-green">
              <Sidenav />
            </div>
            <div className="main-content col col-md-11">
              <div className="row">
                <div className="card branding-bg-light-sea-green">
                  <CardToolbar title="recent observations" />
                  <NotificationTable />
                </div>
                <div className="card branding-bg-light-sea-green">
                  <CardToolbar title="observation trends" />
                  <LineChart />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default MakeObservationPage
