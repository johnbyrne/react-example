import {
  GET_ACTIVE_SIDENAV_TAB,
  SET_ACTIVE_SIDENAV_TAB } from './actionTypes'

let activeSidenavTab = 'maths'

export const getActiveSidenavTab = () => {
  return {
    type: GET_ACTIVE_SIDENAV_TAB
  }
}

export const setActiveSidenavTab = (activeSidenavTab) => {
  return {
    type: SET_ACTIVE_SIDENAV_TAB,
    activeSidenavTab
  }
}
