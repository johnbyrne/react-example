import DashboardPage from './Dashboard/DashboardPage'
import MakeObservationPage from './MakeObservation/MakeObservationPage'

module.exports = { DashboardPage, MakeObservationPage }
