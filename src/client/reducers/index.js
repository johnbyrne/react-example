import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
// import { routeReducer, UPDATE_LOCATION } from 'react-router-redux'

// Asynchronous/services state

import {
  REQUEST_NOTIFICATION_DATA,
  RECEIVE_NOTIFICATION_DATA } from '../services/serviceTypes'

// Pages/components state

import {
  GET_ACTIVE_SIDENAV_TAB,
  SET_ACTIVE_SIDENAV_TAB } from '../pages/MakeObservation/actions/actionTypes'

// Asynchronous/services state

const notificationDataState = (state = {
  isLoading: false,
  notificationData: []
}, action) => {
  switch (action.type) {
    case REQUEST_NOTIFICATION_DATA:
      return Object.assign({}, state, {
        isLoading: true
      })
    case RECEIVE_NOTIFICATION_DATA:
      return Object.assign({}, state, {
        isLoading: false,
        notificationData: action.notificationData || []
      })
    default:
      return state
  }
}

// Pages/components state

const makeObservationsPage_activeSidenavTabState = (state = {
  activeSidenavTab: 'maths'
}, action) => {
  switch (action.type) {
    case GET_ACTIVE_SIDENAV_TAB:
      return state
    case SET_ACTIVE_SIDENAV_TAB:
      return Object.assign({}, state, {
        activeSidenavTab: action.activeSidenavTab
      })
    default:
      return state
  }
}

const rootReducer = combineReducers({
  // Asynchronous/services state
  notificationDataState,
  // Pages/components state
  makeObservationsPage_activeSidenavTabState,
  routing: routerReducer
})

export default rootReducer
